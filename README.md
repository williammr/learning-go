# Learning Go

## Becoming a Go Developer

- Learn the basics
- Build a REST API
- Microservices with gRPC
- Build & Deployment
- Build a CLI tool
- Performance
- Testing
- Frontend Apps with Gin Framework


